#!/bin/sh

# Create a database of common names.

DB=PeopleNames.db

(

cat<<!
DROP TABLE IF EXISTS GivenNames;
DROP TABLE IF EXISTS FamilyNames;
DROP INDEX IF EXISTS IDX_GivenNames_Sex;
DROP VIEW IF EXISTS GirlNames;
DROP VIEW IF EXISTS BoyNames;
CREATE TABLE GivenNames (Name VARCHAR(20), Sex VARCHAR(1));
CREATE TABLE FamilyNames (Name VARCHAR(30));
!

# Insert the girl names
sed '/^ *#/d;/^[ 	]*$/d' sourcdata/GirlNames.txt | while read name
do
	echo "INSERT INTO GivenNames VALUES (\"${name}\", \"F\");"
done

# Insert the boy names
sed '/^ *#/d;/^[ 	]*$/d' sourcdata/BoyNames.txt | while read name
do
	echo "INSERT INTO GivenNames VALUES (\"${name}\", \"M\");"
done

# Insert the family names
sed '/^ *#/d;/^[ 	]*$/d' sourcdata/FamilyNames.txt | while read name
do
	echo "INSERT INTO FamilyNames VALUES (\"${name}\");"
done

# Add boy/girl views and index
cat <<!
CREATE VIEW GirlNames AS SELECT rowid, Name FROM GivenNames WHERE Sex="F";
CREATE VIEW BoyNames AS SELECT rowid, Name FROM GivenNames WHERE Sex="M";
CREATE INDEX IDX_GivenNames_Sex ON GivenNames (Sex);
VACUUM;
.schema
!
) | sqlite3 ${DB}

echo
echo `sqlite3 ${DB} 'select count(*) from Girlnames'` girl names added.
echo `sqlite3 ${DB} 'select count(*) from Boynames'` boy names added.
echo `sqlite3 ${DB} 'select count(*) from Familynames'` family names added.
