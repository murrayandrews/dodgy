#!/bin/sh

# Add state based views to the AU placenames database.

(
for state in `sqlite3 PlaceNames_AU.db 'select distinct AdminCode1 from PlaceNames;'`
do
	echo "CREATE VIEW IF NOT EXISTS PlaceNames_${state} AS SELECT rowid, * FROM PlaceNames WHERE AdminCode1=\"${state}\";"
done
echo "CREATE INDEX IF NOT EXISTS IDX_PlaceNames_AdminCode1 on PlaceNames (AdminCode1);"
echo ".schema"
) | sqlite3 PlaceNames_AU.db
