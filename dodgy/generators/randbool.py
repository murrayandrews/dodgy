"""
Random boolean generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import random

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def randbool(rownum):
    """
    Generate a random boolean.

    Note that we allow the kwargs to shadow min/max builtins in this case as
    'min' and 'max' are the labels we want to use for these params in the YAML
    file.

    :param rownum:      Not used for this generator.
    :type rownum:       int

    :return:            A random bool.
    :rtype:             bool

    """

    return random.choice([True, False])
