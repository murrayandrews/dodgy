"""
Random float generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import random

__author__ = 'Murray Andrews'

# Range limits are a bit arbitrary
MAX = 2**31 - 1
MIN = -MAX - 1


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins, PyUnusedLocal
def randfloat(rownum, min=MIN, max=MAX, precision=None):
    """
    Generate a random float somewhere between min and max (inclusive).

    Note that we allow the kwargs to shadow min/max builtins in this case as
    'min' and 'max' are the labels we want to use for these params in the YAML
    file.

    :param rownum:      Not used for this generator.
    :param min:         Minimum length. Default -2**31
    :param max:         Maximum length. Default 2**31-1.
    :param precision:   Number of decimal places. If None then no
                        rounding is done. Default None.

    :type rownum:       int
    :type min:          float | int
    :type max:          float | int
    :type

    :return:            A random float.
    :rtype:             float

    """

    if not isinstance(min, (int, float)):
        raise ValueError('randfloat: bad min value: {}'.format(min))

    if not isinstance(max, (int, float)):
        raise ValueError('randfloat: bad max value: {}'.format(max))

    if min > max:
        raise ValueError('randfloat: min is greater than max')

    f = random.uniform(min, max)

    return round(f, precision) if precision else f
