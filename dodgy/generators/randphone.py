"""
Random phone number generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import random

__author__ = 'Murray Andrews'

MAXINT = 2 ** 31 - 1
MININT = -MAXINT - 1

# Wash style names through this list
STYLE_SYNONYMS = {
    'mobile': 'mobile',
    'cell': 'mobile',
    'cellular': 'mobile',
    'fixed': 'fixed',
    'pstn': 'fixed',
    'landline': 'fixed'
}

PHONE_STYLES = {
    'AU': {
        'mobile': ('04{} {} {}', 2, 3, 3),  # Format and length of each piece
        'fixed': ('0{} {} {}', 1, 4, 4),
    }
}

COUNTRY_CODES = {
    'AU': 61
}


# ------------------------------------------------------------------------------
def _rand_digit_str(length):
    """
    Generate a random digit string of the specified length - make sure it is
    not all zeros.

    :param length:
    :return:
    """

    return '{0:0{1}}'.format(random.randint(1, 10**length-1), length)


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins, PyUnusedLocal
def randphone(rownum, country='AU', style=None, international=False):
    """
    Generate a random phone number.

    :param rownum:      Not used for this generator.
    :param country:     Two digit ISO 3166 country code. Default AU.
    :param style:       One the available styles for the given country. If None
                        then select a style at random. Default None.
    :param international: If True then return an internal +cc version instead.
                        Default False.

    :type rownum:       int
    :type country:      str
    :type style:        str
    :type international: bool

    :return:            A random phone number formatted as a string.
    :rtype:             str

    """

    try:
        country_styles = PHONE_STYLES[country.upper()]
    except KeyError:
        raise ValueError('randphone: unknown country: {}'.format(country))

    if style:
        try:
            style = STYLE_SYNONYMS[style.lower()]
        except KeyError:
            raise ValueError('randphone: unknown style {}'.format(style))

    else:
        style = random.choice(list(country_styles))

    fmt = country_styles[style]
    phone = fmt[0].format(*[_rand_digit_str(l) for l in fmt[1:]])

    return '+{} {}'.format(COUNTRY_CODES[country], phone[1:]) if international else phone
