"""
Random string generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import string
from random import randint, choice

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins, PyUnusedLocal
def randstr(rownum, min=1, max=20, case='mixed'):
    """
    Generate a pseudo-random string with length somewhere between min and max
    (inclusive). Characters are selected from ASCII letters.

    Note that we allow the kwargs to shadow min/max builtins in this case as
    'min' and 'max' are the labels we want to use for these params in the YAML
    file.

    DO NOT use this for cryptographic / security purposes.

    :param rownum:      Not used for this generator.
    :param min:         Minimum length
    :param max:         Maximum length
    :param case:        Can be one of 'mixed' (mixed case), 'upper' (all
                        upper case), 'lower' (all lower case) or 'initcap'
                        (first letter upper case, all others lower).
                        Default is 'mixed'

    :type rownum:       int
    :type min:          int
    :type max:          int
    :type case:         str

    :return:            A random string chosen from ASCII alpha chars.
    :rtype:             str

    """

    if case in ('lower', 'initcap'):
        source = string.ascii_lowercase
    elif case == 'upper':
        source = string.ascii_uppercase
    elif case == 'mixed':
        source = string.ascii_letters
    else:
        raise ValueError('Unknown randstr case: {}'.format(case))

    s = ''.join([choice(source) for _ in range(randint(min, max))])

    return s[0].upper() + s[1:] if case == 'initcap' else s
