"""
Random email address generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

from .randstr import randstr

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def randemail(rownum, domain='dodgy.zz'):
    """
    Generate a pseudo-random email address. These won't be useable to send
    email. Probably.

    :param rownum:      Not used for this generator.
    :param domain:      Domain name for the email address. Default: dodgy.zz
    :type rownum:       int
    :type domain:       str

    :return:            A random email address.
    :rtype:             str

    """

    return '{}@{}.{}'.format(
        randstr(rownum, 4, 8, case='lower'),
        randstr(rownum, 2, 4, case='lower'),
        domain
    )
