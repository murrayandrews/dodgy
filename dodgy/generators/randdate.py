"""
Random date generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import random
from datetime import datetime, date, timedelta

__author__ = 'Murray Andrews'

_parsed_dates = {}

date_formats = (
    '%d %b %Y',  # 28 Feb 2016
    '%d %B %Y',  # 28 February 2016
    '%d-%b-%Y',  # 28-Feb-2016
    '%d/%m/%Y',  # 28/02/2016
    '%d-%m-%Y',  # 28-02-2016
    '%d.%m.%Y',  # 28.02.2016
    '%Y-%m-%d',  # 2016-02-28
    '%Y%m%d',    # 20160228
)


# ------------------------------------------------------------------------------
def parse_date(s):
    """
    Attempt to parse the string s into a date object. Once a date is parsed it
    is cached to avoid the overhead of reparsing the same string twice.

    :param s:           A string representing a date. It can be in one of a
                        number of different formats - see date_formats above.
                        In addition, 'now' and 'today' are accepted with the
                        obvious meaning.

                        Note that we do NOT accept the locale specifc %x format
                        of strptime due to its ambiguity.

    :type s:            str

    :return:            A date object.
    :rtype:             date

    :raise ValueError:  If s cannot be parsed to a date.
    """

    d = _parsed_dates.get(s)
    if d:
        return d

    if isinstance(s, int):
        s = str(s)

    if s.lower() in ('today', 'now'):
        _parsed_dates[s] = d = datetime.now().date()
        return d

    for fmt in date_formats:
        try:
            d = datetime.strptime(s, fmt)
        except ValueError:
            continue
        else:
            _parsed_dates[s] = d = d.date()
            return d

    raise ValueError('cannot parse date: {}'.format(s))


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins, PyUnusedLocal
def randdate(rownum, min=None, max=None):
    """
    Generate a random date somewhere between min and max (inclusive).

    Note that we allow the kwargs to shadow min/max builtins in this case as
    'min' and 'max' are the labels we want to use for these params in the YAML
    file.

    :param rownum:      Not used for this generator.
    :param min:         Minimum date (as a string). It can be in one of a
                        number of different formats - see date_formats above.
                        In addition, 'now' and 'today' are accepted with the
                        obvious meaning.

                        Note that we do NOT accept the locale specifc %x format
                        of strptime due to its ambiguity.

                        If None, use the minimum value allowed by datetime.
                        Default None.

    :param max:         Maximum date as a string. Same format options as min.
                        If None, use the maximum value allowed by datetime.
                        Default None.

    :type rownum:       int
    :type min:          str
    :type max:          str

    :return:            A random date
    :rtype:             date

    """

    if not min:
        min_d = datetime.min.date()
    else:
        try:
            min_d = parse_date(min)
        except ValueError as e:
            raise ValueError('randdate: {}'.format(e))

    if not max:
        max_d = datetime.max.date()
    else:
        try:
            max_d = parse_date(max)
        except ValueError as e:
            raise ValueError('randdate: {}'.format(e))

    if min_d > max_d:
        raise ValueError('randdate: min is greater than max')

    return min_d + timedelta(days=random.randint(0, (max_d - min_d).days))
