"""
Selection generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import os
import random
import re
import sqlite3

__author__ = 'Murray Andrews'

DODGYPATHENV = 'DODGYPATH'

# db_cache contains one entry per database file. Each value is a tuple:
#    ( cursor, { table_name1: [ rowids] } )
# Note that the second entry in the tuple is a dictionary of table names
# with the value for each tablename being the rowids in that table.
# This can be large but selection by rowid is very fast.
db_cache = {}

# db_record_cache contains one entry per database file. Each value is a dict:
#   { table_name: (rownum, most_recently_retrieved_record) }
# The most recently_retrieved_record is itself a dict.
db_record_cache = {}

# db_schema_cache contains one entry per database file. Each value is a dict:
#   { table_name: [list of columns] }
# The most recently_retrieved_record is itself a dict.
db_schema_cache = {}

# file_cache is a dict {filename : values in file}
file_cache = {}


# ------------------------------------------------------------------------------
# TODO: Use this
def find_in_path(filename, path=None):
    """
    Find a file in a path. If an absolute path is provided in filename then it
    is merely checked for existance and the absolute path will be returned if it
    exists. Otherwise the path will be searched for the file and, if it exists in
    the path, the absolute path will be returned.

    :param filename:    The name of the file to find.
    :param path:        The path. May be either a string of : separated
                        directories or an iterable of dir names. An empty path
                        ('', [] or None) is treated as current directory only.
                        Default: None.
    :type filename:     str
    :return:            The absolute path of the file if found otherwise None.
    :rtype:             str

    """

    if not filename:
        raise ValueError('filename must be specified')

    if os.path.isabs(filename):
        return filename if os.path.exists(filename) else None

    if not path:
        path = ['.']
    elif isinstance(path, str):
        path = path.split(':')

    for d in path:
        d = os.path.expanduser(d)
        p = os.path.join(d, filename)
        if os.path.exists(p):
            return os.path.abspath(p)

    return None


# ------------------------------------------------------------------------------
def select_db(rownum, dbase, table, column, path=None):
    """
    Select an entry a random from the specified database table.

    :param rownum:      Allows us to cache SQLite data across multiple calls
                        within the same output row.
    :param dbase:       Name of an SQLite3 database file.
    :param table:       Name of a table in dbase.
    :param column:      Name of a column in the table.
    :param path:        Search path for the file. If None then path is current
                        directory only. Default None.

    :type rownum:       int
    :type dbase:        str
    :type table:        str
    :type column:       str
    :type path:         str
    :return:            A randomly chosen element from the table.
    :rtype:             T

    """

    # ----------------------------------------
    # See if we already have a database cursor. The cache contains a tuple
    # consisting of (cursor, dict[table names --> rowids]) for each database.

    cursor, table_info = db_cache.get(dbase, (None, {}))
    if not cursor:
        located_dbase = find_in_path(dbase, path=path)
        if not located_dbase:
            raise Exception('Cannot locate selection database: {}'.format(dbase))
        conn = sqlite3.connect(located_dbase)
        cursor = conn.cursor()
        db_cache[dbase] = (cursor, {})
        db_record_cache[dbase] = {}
        db_schema_cache[dbase] = {}

    # ----------------------------------------
    # Have a cursor, see if we have rowid info for this table.

    table_rowids = table_info.get(table)
    if table_rowids is None:
        # Make sure no nasties in table or column name
        if not re.match(r'^\w+$', table):
            raise ValueError('Invalid table name: {}'.format(table))

        if not re.match(r'^\w+$', column):
            raise ValueError('Invalid column name: {}'.format(column))

        # Get table rowids. We use this to fetch a random element by rowid.
        # We don't use the sqlite rand() function because it cannot be seeded.

        cursor.execute('SELECT rowid FROM {}'.format(table))
        table_rowids = [r[0] for r in cursor.fetchall() if r[0] is not None]

        if not table_rowids:
            raise Exception(
                'Cannot select from empty table or table with no rowids: {}'.format(table))

        # Update the cache
        table_info[table] = table_rowids
        db_cache[dbase] = (cursor, table_info)

        # Get the table schema
        cursor.execute('SELECT * FROM {} LIMIT 0'.format(table))
        db_schema_cache[dbase][table] = [f[0] for f in cursor.description]

    # ----------------------------------------
    # Now have rowid and schema info for the table. Fetch an item at random.

    if table in db_record_cache[dbase] and rownum == db_record_cache[dbase][table][0]:
        # Use previously cached value
        record = db_record_cache[dbase][table][1]
    else:
        # No cached value or it applied to an earlier output row.
        cursor.execute('SELECT * FROM {} WHERE rowid=?'.format(table),
                       (random.choice(table_rowids),))
        record = dict(zip(db_schema_cache[dbase][table], cursor.fetchone()))

        db_record_cache[dbase][table] = (rownum, record)

    return record[column]


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins
def select_file(file, path=None):
    """
    Select an entry at random from the specified file. File contents are
    cached in memory.

    :param file:        Name of a file containing one value per line. Empty
                        lines and those starting with # are ignored.
    :param path:        Search path for the file. If None then path is current
                        directory only. Default None.
    :type file:         str
    :type path:         str
    :return:            A randomly chosen element from the file.
    :rtype:             str
    """

    values = file_cache.get(file)
    if not values:
        # Need to load the file.

        located_file = find_in_path(file, path=path)
        if not located_file:
            raise Exception('Cannot locate selection file: {}'.format(file))
        values = []
        with open(located_file, 'r') as fp:
            for l in fp.readlines():
                v = l.strip()
                if not v or v.startswith('#'):
                    continue
                values.append(v)

        if not values:
            raise Exception('Selection file contains no values: {}'.format(located_file))

        file_cache[file] = values

    return random.choice(values)


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins
def select(rownum, values=None, dbase=None, table=None, column=None, file=None):
    """
    Select an item at random from a list. The list can be provided in one of the
    following forms:

        - a list of static values (using the 'values' parameter).
        - a specified column in a SQLite3 database (using the dbase, table and
          column parameters).
        - a text file with one value per line (using the file parameter).

    :param rownum:      This is used to determine when a new output row is
                        being started. A record retrieved from a database table
                        is cached for the duration of processing on that row.
                        This has two benefits - we only retrieve a single
                        database record from a given select table for each
                        output record and also if multiple columns from the
                        select table need to be used in a single output row,
                        they all match.
    :param values:      A list of values. Must contain at least one value.
    :param dbase:       Name of an SQLite3 database file.
    :param table:       Name of a table in dbase.
    :param column:      Name of a column in the table.
    :param file:        Name of a file containing selection values (one per line).

    :type rownum:       int
    :type values:       list
    :type dbase:        str
    :type table:        str
    :type column:       str
    :type file:         str

    :return:            A randomly chosen element from the list
    :rtype:             T

    """

    if values is not None:
        # Try for a static list first
        if not values or not isinstance(values, list):
            raise Exception('Selection list is empty or not a list')
        selection = random.choice(values)

    elif dbase is not None:
        # Try for an SQLite3 dbase
        selection = select_db(rownum, dbase, table, column,
                              path=os.environ.get(DODGYPATHENV))

    elif file is not None:
        # Try for a file containing values.
        selection = select_file(file, path=os.environ.get(DODGYPATHENV))

    else:
        raise Exception('Selection source not specified')

    return selection.encode('utf-8') if isinstance(selection, unicode) else selection
