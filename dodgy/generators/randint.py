"""
Random int generator for dodgy.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

import random

__author__ = 'Murray Andrews'

MAXINT = 2**31 - 1
MININT = -MAXINT - 1


# ------------------------------------------------------------------------------
# noinspection PyShadowingBuiltins, PyUnusedLocal
def randint(rownum, min=MININT, max=MAXINT):
    """
    Generate a random integer somewhere between min and max (inclusive).

    Note that we allow the kwargs to shadow min/max builtins in this case as
    'min' and 'max' are the labels we want to use for these params in the YAML
    file.

    :param rownum:      Not used for this generator.
    :param min:         Minimum length. Default -2**31
    :param max:         Maximum length. Default 2**31-1.

    :type rownum:       int
    :type min:          int
    :type max:          int

    :return:            A random int.
    :rtype:             int

    """

    if not isinstance(min, (int, float)):
        raise ValueError('randint: bad min value: {}'.format(min))

    if not isinstance(max, (int, float)):
        raise ValueError('randint: bad max value: {}'.format(max))

    if min > max:
        raise ValueError('randint: min is greater than max')

    return random.randint(min, max)
