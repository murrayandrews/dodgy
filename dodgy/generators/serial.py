"""
Generator for serial numbers. The same value will never get returned twice.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

__author__ = 'Murray Andrews'

_serial = {}
_incr = {}


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def serial(rownum, start=0, incr=1, name=None):
    """
    Generate the next serial number in a sequence. Sequences are named and are
    disjoint.

    :param rownum:  Not used for this generator.
    :param start:   Initial value. Default is 0.
    :param incr:    Increment for each value. Cannot be 0. Default 1.
    :param name:    Name of the serial. Each named serial counter is
                    independent. Default 'default'.

    :type rownum:   int
    :type start:    int
    :type incr:     int

    :return:        The next integer in the sequence.
    :rtype:         int

    """

    global _serial, _incr

    if name not in _serial:
        _serial[name] = int(start)
        if not incr:
            raise ValueError('Serial number increment cannot be 0')
        _incr[name] = int(incr)
    else:
        _serial[name] += _incr[name]

    return _serial[name]
