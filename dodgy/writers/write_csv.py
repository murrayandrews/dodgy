"""
Write a table of data as a CSV file.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

from __future__ import print_function
import csv
import sys

__author__ = 'Murray Andrews'

dialects = csv.list_dialects()


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def write_csv(n, tablespec, headers=True, outfile=sys.stdout, dialect='excel',
              **kwargs):
    """
    Write n rows of data in CSV format.

    :param n:           Number of rows. If n is 0 and headers is True then only
                        the header is written. If n is 0 and headers is False
                        then you're not going to see much action.
    :param tablespec:   A TableSpec object used to generate the data.
    :param headers:     If True, print headers. Default True.
    :param outfile:     Stream to write to.
    :param dialect      CSV dialect. Anything accepted by the csv module.
    :param kwargs:      Other keyword args are ignored.

    :type n:            int
    :type tablespec:    TableSpec
    :type headers:      bool
    :type outfile:      file
    :type dialect:      str
    """

    writer = csv.writer(outfile, dialect=dialect)

    if headers:
        writer.writerow([cs.name for cs in tablespec])
    if n:
        writer.writerows(tablespec.make_rows(n))
