"""
Write a table of data as SQL INSERT statements. This is very naive in handling
types and escaping of special chars.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

from __future__ import print_function

import sys
from datetime import date

__author__ = 'Murray Andrews'

# Do a commit after this many rows. 0 means one commit surrounding all rows.
# -1 means no commits.
COMMIT_FREQUENCY = 10000

# Each dialect is a dictionary of type --> encoder function
DIALECTS = {
    'sqlite': {
        type(True): lambda b: '1' if b else '0',  # No native boolean in sqlite
    },
    'generic': {
        type(0): str,  # Int
        type(0.1): str,  # Float
        type(None): lambda _: 'NULL',  # None
        type('abc'): lambda s: "'{}'".format(s),  # String
        type(True): lambda b: 'true' if b else 'false',  # Boolean
        type(u'nicode'): lambda s: "'{}'".format(s),
        date: lambda x: x.strftime("'%Y-%m-%d'"),
    }
}

dialects = sorted(DIALECTS)


# ------------------------------------------------------------------------------
def add_cli_args(argp):
    """
    Add any format specific CLI arguments. These MUST be of the form:

        --sql-xxxx

    and the "dest" var must be sql_xxxx

    :param argp:        An argparse Argument Parser.
    :type argp:         argparse.ArgumentParser
    """

    argp.add_argument('--sql-commit', dest='sql_commit', type=int, default=0,
                      help='For sql format, commit changes after this many rows. '
                           'If 0 then place all row INSERTs inside a single transaction. '
                           'If -1 then do not use transactions at all.')


# ------------------------------------------------------------------------------
def _sql_encode(value, dialect='generic'):
    """
    Encode a value as a string in way that it can be used in a SQL INSERT
    statement. Feeble attempt to handle different SQL dialects.

    :param value:       The value to encode.
    :param dialect:     SQL dialect - must be one of the keys in the DIALECTS
                        dict. Default 'generic'.
    :type value:        T
    :type dialect:      str

    :return:            The value in string format.
    :rtype:             str

    :raise ValueError:  If the dialect is not known or the value cannot be
                        encoded.
    """

    if not dialect:
        dialect = 'generic'
    try:
        dialect_encoders = DIALECTS[dialect]
    except KeyError:
        raise ValueError('Unknown dialect: {}'.format(dialect))

    t = type(value)

    encoder = dialect_encoders.get(t, DIALECTS['generic'].get(t))
    if not encoder:
        raise ValueError('Cannot encode {}: no encoder available for {}'.format(value, t))

    # noinspection PyCallingNonCallable
    return encoder(value)


# ------------------------------------------------------------------------------
def write_sql(n, tablespec, headers=True, outfile=sys.stdout,
              dialect='generic', sql_commit=COMMIT_FREQUENCY):
    """
    Write n rows of data as SQL insert statements.

    :param n:           Number of rows. If n is 0 and headers is True then only
                        the header is written.
    :param tablespec:   A TableSpec object used to generate the data.
    :param headers:     If True, print headers. Default True.
    :param outfile:     Stream to write to.
    :param dialect:     Feeble attempt to handle different SQL dialects. See
                        DIALECTS dict for available dialects. Default 'generic'.
    :param sql_commit:  Do a commit after this many rows. 0 means one commit
                        surrounding all rows. -1 means no commits.
                        Default COMMIT_FREQUENCY.

    :type n:            int
    :type tablespec:    TableSpec
    :type headers:      bool
    :type outfile:      file
    :type dialect:      str
    :type sql_commit:   int
    """

    if headers:
        print('DROP TABLE IF EXISTS {};'.format(tablespec.name))
        create_stmt = 'CREATE TABLE {} ({}\n);'.format(
            tablespec.name,
            ','.join(['\n  {col.name} {col.sqltype}'.format(col=cs) for cs in tablespec])
        )
        print(create_stmt, file=outfile)

    if not n:
        return

    commit_pending = False

    if sql_commit == 0:
        # One transaction surrounds all rows
        print('BEGIN TRANSACTION;', file=outfile)
        commit_pending = True

    insert_stmt = 'INSERT INTO ' + tablespec.name + ' VALUES ({});'

    row_counter = 0
    for r in tablespec.make_rows(n):
        if sql_commit > 0 and row_counter % sql_commit == 0:
            # Time to start a new transaction
            if commit_pending:
                print('COMMIT;', file=outfile)

            print('BEGIN TRANSACTION;', file=outfile)
            commit_pending = True

        row_counter += 1

        encoded_r = []
        for field in r:
            encoded_r.append(_sql_encode(field, dialect))
        print(insert_stmt.format(', '.join(encoded_r)), file=outfile)

    if commit_pending:
        print('COMMIT;', file=outfile)
