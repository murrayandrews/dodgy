"""
Write a table of data in raw Python format.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

from __future__ import print_function
import sys

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def write_raw(n, tablespec, headers=True, outfile=sys.stdout, **kwargs):
    """
    Write n rows of data in raw (Python) format.

    :param n:           Number of rows. If n is 0 and headers is True then only
                        the header is written.
    :param tablespec:   A TableSpec object used to generate the data.
    :param headers:     If True, print headers. Default True.
    :param outfile:     Stream to write to.
    :param kwargs:      Other keyword args are ignored.

    :type n:            int
    :type tablespec:    TableSpec
    :type headers:      bool
    :type outfile:      file
    """

    if headers:
        print([cs.name for cs in tablespec], file=outfile)
    for r in tablespec.make_rows(n):
        print(r, file=outfile)
