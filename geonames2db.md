# geonames2db(1) -- Convert a GeoNames data file to a SQLite3 database

## SYNOPSIS

`geonames2db` sqlite-db-file < geonames_data_file

## DESCRIPTION

Geonames2db converts a country datafile from GeoNames (<http://www.geonames.org>) into an
SQLite3 database for use with the _select_ data generator in dodgy(1).


## OPTIONS

*   `sqlite-db-file`:
    Name of the database file. If it doesn't exist, it will be created. If it does exist
    any existing data corresponding to countries in the geonames input file will first
    be removed and the new data added.

## SOURCE DATA

Source datafiles are available from <http://download.geonames.org/export/zip/>.

There are individual data files for many countries as well as an _all countries_ file.

The source files are tab separated text files, one record per line. Each line contains
the following fields:

*   `country code`:
    ISO country code, 2 characters

*   `postal code`:

*   `place name`:

*   `admin name1`:
    1st order subdivision (state) varchar(100)

*   `admin code1`:
    1st order subdivision (state) varchar(20)

*   `admin name2`:
    2nd order subdivision (county/province) varchar(100)

*   `admin code2`:
    2nd order subdivision (county/province) varchar(20)

*   `admin name3`:
    3rd order subdivision (community) varchar(100)

*   `admin code3`:
    3rd order subdivision (community) varchar(20)

*   `latitude`:
    Estimated latitude (wgs84)

*   `longitude`:
    Estimated longitude (wgs84)

*   `accuracy`:
    Accuracy of lat/lng from 1=estimated to 6=centroid

## DATABASE SCHEMA

Geonames2db creates the following schema:

```
CREATE TABLE PlaceNames (
     CountryCode VARCHAR(2),
     PostCode    VARCHAR(20),
     PlaceName   VARCHAR(180),
     AdminName1  VARCHAR(100),
     AdminCode1  VARCHAR(20),
     AdminName2  VARCHAR(100),
     AdminCode2  VARCHAR(20),
     AdminName3  VARCHAR(100),
     AdminCode3  VARCHAR(20),
     Latitude    FLOAT,
     Longitude   FLOAT,
     Accuracy    INT
);
```
## SEE ALSO

dodgy(1)

## LICENCE

Geonames2b is provided under the terms of Apache 2.0 licence.

The GeoNames data files are generally free but are subject to their
own terms. See  <http://www.geonames.org> for details.

## AUTHOR

Murray Andrews
