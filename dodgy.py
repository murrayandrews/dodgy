#!/usr/bin/env python

"""
Generate random tabular data. The required format is specified
in a YAML file.

All rights reserved (Murray Andrews 2015)

See licence.txt

"""

from __future__ import print_function

import os
import argparse
import importlib
import random
import re
import sys
from os.path import basename

import yaml


FORMATS = ['csv', 'raw', 'sql']

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])
NROWS = 10  # Default number of rows
DODGYPATHENV = 'DODGYPATH'

# These are the key names in the YAML file. Note we allow synonyms.

YK_PATH = ['path', 'dodgypath']
YK_SEED = ['seed']
YK_TABLE = ['table']
YK_COLS = ['cols', 'columns', 'fields']
YK_NAME = ['name', 'n', 'col', 'column', 'c']  # Column name
YK_TYPE = ['type', 't']  # Column type
YK_GEN = ['generator', 'gen', 'g']  # Generator for the colum
YK_PARAMS = ['params', 'p']  # Parameters for the generator


# -------------------------------------------------------------------------------
def import_by_name(name, parent=None):
    """
    Import a named module from within the named parent.

    :param name:            The name of the sender module.
    :param parent:          Name of parent module. Default None.

    :type name:             str
    :type parent:           str

    :return:                The sender module.
    :rtype:                 module

    :raise ImportError:     If the import fails.
    """

    if parent:
        name = parent + '.' + name

    return importlib.import_module(name)


# ------------------------------------------------------------------------------
def mget(d, keys, default=None):
    """
    Lookup each of the keys in the dict d and return the first non-None value or
    the default if none of the keys have a non-None value.

    :param d:           A dict.
    :param keys:        An iterable containing possible keys for dict d.
    :param default:     Value to return if none of the keys are present.
                        Default None.
    :type d:            dict
    :type keys:         list | tuple
    :type default:      T

    :return:            The value for the first avaiable key of the default.
    :rtype:             T
    """

    for k in keys:
        v = d.get(k)
        if v is not None:
            return v

    return default


# ------------------------------------------------------------------------------
class TableSpec(list):
    """

    """

    table_no = 0  # Used for auto named tables.

    # --------------------------------------------------------------------------
    @staticmethod
    def check_name(name):
        """
        Make sure a name (e.g. table or column name) is valid (ie. only contains
        alphanumeric and _).
        :param name:        Name to check.
        :type name:         str

        :raise ValueError:  If name is invalid

        """

        if not re.match(r'^\w+$', name):
            raise ValueError('Invalid name: {}'.format(name))

    # --------------------------------------------------------------------------
    def __init__(self, tablespec):
        """
        Construct a table spec. Basically a list of column specs with some table
        level attributes.

        :param tablespec:   A dict that must contain a 'cols' or 'fields' key.
                            It may also contain a 'name' key. If no table name
                            is present, one will be assigned.
        :type tablespec:    dict[str, T]

        :raise ValueError:  If tablespec is not a dict or no column specs can
                            be found.
        """

        super(TableSpec, self).__init__()

        if not isinstance(tablespec, dict):
            raise ValueError('tablespec must be a dict not {}'.format(type(tablespec)))

        # Extract or assign a table name
        self.name = mget(tablespec, YK_NAME)
        if self.name:
            TableSpec.check_name(self.name)
        else:
            self.__class__.table_no += 1
            self.name = 'table_{}'.format(self.__class__.table_no)

        # Analyse and store the column specs
        colspecs = mget(tablespec, YK_COLS)
        if not isinstance(colspecs, list):
            raise ValueError('{} key must be a list'.format('/'.join(YK_COLS)))

        for col in colspecs:
            self.append(ColSpec(col))

    # --------------------------------------------------------------------------
    def __str__(self):

        return '------\nTable: {}\n---\n'.format(self.name)\
               + '\n---\n'.join([str(c) for c in self])

    # --------------------------------------------------------------------------
    def make_rows(self, n):
        """
        Generator to provide rows of data for the table.

        :param n:           The number of rows to product
        :type n:            int

        :return:            A generator of values - each element corresponding
                            to the column specifier in the same position.

        """

        for rownum in range(n):
            r = []
            for cs in self:
                r.append(cs(rownum))
            yield r

    # --------------------------------------------------------------------------
    def write_rows(self, n, fmt, headers=True, outfile=sys.stdout, **kwargs):
        """
        Write the rows of generated data in the specified format.

        :param n:           The number of rows to product
        :param fmt:         Output format. This is used to look for an output
                            handler in the module writers.
        :param headers:     If True, print headers. Default True.
        :param outfile:     Stream to write to.
        :param kwargs:      Values depend on underlying format writer.

        :type n:            int
        :type fmt:          str
        :type headers:      bool
        :type outfile:      file

        """

        writer_name = 'write_' + fmt
        writer_module = import_by_name(writer_name, 'dodgy.writers')
        try:
            writer = getattr(writer_module, writer_name)
        except AttributeError:
            raise Exception('Cannot find writer function {} in dodgy.writers.{}'.format(
                writer_name, writer_name))

        writer(n, tablespec=self, headers=headers, outfile=outfile, **kwargs)


# ------------------------------------------------------------------------------
class ColSpec(object):
    """
    Store a column spec.
    """

    # --------------------------------------------------------------------------
    def __init__(self, d):
        """
        Consruct a column spec. Check the sample YAML file for more info.

        :param d:       A dict that must contain name, type and generator keys
                        and may also contain a params key.
        :raise ValueError: If the required keys are missing or malformed.
        """

        self.name = mget(d, YK_NAME)
        if not self.name or not isinstance(self.name, str):
            raise ValueError('Missing or malformed name attribute: {}'.format(d))

        TableSpec.check_name(self.name)

        self.sqltype = mget(d, YK_TYPE)
        if not self.sqltype or not isinstance(self.sqltype, str):
            raise ValueError('{}: Missing or malformed type attribute'.format(self.name))

        self.generator_name = mget(d, YK_GEN)
        if not self.generator_name or not isinstance(self.generator_name, str):
            raise ValueError('{}: Missing or malformed generator attribute'.format(self.name))

        # Try to locate the generator
        try:
            generator_module = import_by_name(self.generator_name, 'dodgy.generators')
        except ImportError:
            raise ValueError('{}: Cannot locate generator {}'.format(
                self.name, self.generator_name))

        try:
            self.generator = getattr(generator_module, self.generator_name)
        except AttributeError:
            raise Exception('{}: Cannot find {} function in dodgygen.{}'.format(
                self.name, self.generator_name, self.generator_name))

        self.params = mget(d, YK_PARAMS, {})
        if not isinstance(self.params, dict):
            raise ValueError('{}: Params must be a dict'.format(self.name))

    # --------------------------------------------------------------------------
    def __call__(self, rowid):
        """
        Make the instance callable to generate a value according to the colspec.

        :param rowid:       Some generators may use this information to maintain
                            state information for multiple calls within a row.
        :type rowid:        int
        :return:            A generated value.
        :rtype:             T
        """

        return self.generator(rowid, **self.params)

    # --------------------------------------------------------------------------
    def __str__(self):

        return 'Column: {x.name}\nType: {x.sqltype}\n' \
               'Generator: {x.generator_name}\nParams: {x.params}'.format(x=self)


# ------------------------------------------------------------------------------
def main():
    """
    Main.

    :return:    Exit status.
    :rtype:     int
    """

    argp = argparse.ArgumentParser(
        description='Generate random tabular data.',
        prog=PROG
    )

    argp.add_argument('-r', '--rows', type=int, default=NROWS,
                      help='Number of rows to generate. Default {}.'.format(NROWS))
    argp.add_argument('-f', '--format', choices=FORMATS, default=FORMATS[0],
                      help='Output format. Default {}.'.format(FORMATS[0]))
    argp.add_argument('-n', '--noheader', dest='header', action='store_false', default=True,
                      help='Suppress any header in the output. Some output formats'
                           'may not produce a header.')
    argp.add_argument('-p', '--path', action='store',
                      help='Specify the path for locating source data files used by '
                           'generators. Overrides any value in the config file and '
                           'the {} environment variable.'.format(DODGYPATHENV))
    argp.add_argument('-s', '--seed', action='store',
                      help='Seed the random number generator. Can be int or string.')
    argp.add_argument('config', metavar='config.yaml', type=argparse.FileType('r'),
                      help='YAML file containing required tmp data config')

    # Check each of the available writers for their various dialects
    writers = {f: import_by_name('write_' + f, 'dodgy.writers') for f in FORMATS}
    dialects = {f: getattr(writers[f], 'dialects', []) for f in FORMATS}

    all_dialects = []
    fmt_specific_dialects = []
    for f in sorted(dialects):
        all_dialects += dialects[f]
        if dialects[f]:
            fmt_specific_dialects.append('{}: {}'.format(f, ', '.join(dialects[f])))
    argp.add_argument('--dialect', action='store', choices=all_dialects,
                      help='Dialect options: {}'.format('. '.join(fmt_specific_dialects)))

    # Allow each writer to add its own command line args.
    for w in writers.values():
        try:
            arg_adder = getattr(w, 'add_cli_args')
        except AttributeError:
            continue
        arg_adder(argp)

    args = argp.parse_args()

    # ----------------------------------------
    # Load the schema config
    try:
        conf = yaml.safe_load(args.config)
    except Exception as e:
        raise Exception('Cannot load {}: {}'.format(args.config.name, e))

    # ----------------------------------------
    # Look for a seed for the random number generator. Command line arg
    # overrides a value from the YAML file.
    if args.seed is not None:
        try:
            seed = int(args.seed)
        except ValueError:
            seed = args.seed
    else:
        seed = mget(conf, YK_SEED)

    random.seed(seed)

    # ----------------------------------------
    # Setup search path for source data files used by some generators.
    if args.path is not None:
        os.environ[DODGYPATHENV] = args.path
    else:
        path = mget(conf, YK_PATH)
        if path:
            os.environ[DODGYPATHENV] = path

    # ----------------------------------------
    # Analyse table spec
    t = mget(conf, YK_TABLE)
    if not t:
        raise Exception('{}: {} key required.'.format(args.config.name, '/'.join(YK_TABLE)))
    tablespec = TableSpec(t)

    # ----------------------------------------
    # Generate and write data rows

    format_specific_args = {arg: getattr(args, arg)
                            for arg in dir(args) if arg.startswith(args.format+'_')}
    tablespec.write_rows(n=args.rows, fmt=args.format, headers=args.header,
                         dialect=args.dialect, **format_specific_args)


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    try:
        exit(main())
    except Exception as ex:
        print('{}: {}'.format(PROG, ex), file=sys.stderr)
        exit(1)
