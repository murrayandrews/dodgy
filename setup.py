from distutils.core import setup

setup(
    name='dodgy',
    version='1.0',
    packages=['dodgy', 'dodgy.generators', 'dodgy.writers'],
    url='https://bitbucket.org/murrayandrews/dodgy',
    license='Apache 2.0',
    author='Murray Andrews',
    author_email='',
    description='Generate random data for database tables'
)
