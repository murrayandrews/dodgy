# Welcome to Dodgy

Dodgy is a basic, easy to use, generator of random test data for database tables.

While it is extensible, it is not a _framework_ or a _module_ and is ready to
use, out of the box just be creating a YAML configuration file.

For more information, check out the manual entries and the sample config file.

* [dodgy manual](dodgy.md)
* [geonames2db manual](geonames2db.md)
* [sample config](sample1.yaml)

Sorry but there is no proper installer yet.

## Requirements

Python 2.7

Not tested under Windows.