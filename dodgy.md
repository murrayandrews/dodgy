# dodgy (1) -- Generate random data for database tables

## SYNOPSIS

`dodgy` options ...

## DESCRIPTION

Dodgy is a basic, easy to use, generator of random test data for database tables.

While it is extensible, it is not a _framework_ or a _module_ and is ready to
use, out of the box just by creating a configuration file (see [SCHEMA CONFIGURATION][]).

The schema for the required table of data is specified in a YAML configuration
file.

## OPTIONS

*   `-c` <config.yaml>, `--config` <config.yaml>:
    Specify the YAML file containing the table schema. Required.

*   `-f` <format>, `--format` <format>:
    Specify the output format for the data. Options are <raw>, <csv> and <sql>.
    Default is <csv>. See also `--dialect`.

*   `-h`, `--help`:
    Print help and exit.

*  `-n`, `--noheader`:
    Don't include a header. The nature of the header is output format dependent.

*   `-p` <path>, `--path` <path>:
    Specify the path for locating source data files used by generators. Overrides
    any value in the configuration file and the DODGYPATH environment variable.

*  `-r` <n>, `--rows` <n>:
    Produce <n> rows of data. Default is 10. If only the header is required, set
    <n> to 0. If `--noheader` and `--rows` <0> are used, then the YAML configuration
    file will be validated but no other output is produced.

*  `-s` <seed>, `--seed` <seed>:
    Specify the seed for the random number generator. <seed> can be either an integer
    or a string. This allows the same data to be generated across multiple runs. If
    not specified, then a more-or-less random seed is chosen by the system. See also
    the `seed` key in the configuration file. If both are specified, the command line
    option takes precedence.

*   `--dialect` <dialect>:
    Specify a dialect for the given output format. See `-f`, `--format`. Use the `-h`,
    `--help` option to list available dialects for each format.

*   `--sql-commit` <n>:
    For the sql output format, commit changes every <n> row INSERTs. If <n> is 0
    then all row INSERTs are done inside a single transaction. If <n> is -1 then do
    not use transactions at all (which can make running the SQL script very slow).
    Default is 0.
   

## SCHEMA CONFIGURATION

The schema configuration file is a YAML file containing the following major keys.
Those marked with * are mandatory.

*   `path` | `dodgypath`:
    Path for locating source data files used by generators. Overrides any value
    in the DODGYPATH environment variable and can, in turn, be overridden by the
    `-p`, `--path` command line option.

*   `seed`:
    A string or integer seed for the random number generator used to create test
    data. A command line value will override this value.

*   `table` *:
    Required. The table specification. See below.

### Table Specification

The table specification consists of the following keys (note some synonys are
accepted for some key names).
Those marked with * are mandatory.

*   `name`:
    Table name. This is used in some output formats (e.g. sql). It must only contain
    alpha-numerics and underscores. If not specified, then a table name will be assigned
    automatically.

*   `cols` | `columns` | `fields` *:
    A list of column specifications (see below). Values will be generated in the order
    in which the column specifications occur.

### Column Specification

The table specification contains a list of column specifications, each of which has the
following keys.
Those marked with * are mandatory.

*   `name` | `n` *:
    Column name. Alphanumerics and underscores only.

*   `type` | `t` *:
    An SQL type. e.g. `INTEGER`, `VARCHAR(20)`.

*   `generator` | `gen` | `g` *:
    Name of the generator function for this column. See [GENERATOR FUNCTIONS][]

*   `params` | `p`:
    Parameters required by the generator function. If present, must be a dictionary/hash
    of parameter values. Parameters are generator specific. See below.

## GENERATOR FUNCTIONS

Dodgy comes with a number of data generator functions supplied. New functions,
written in Python, can be added as required. The built-in generators, and their
associated parameters, are listed below. Mandatory parameters are marked with *.

### randbool

Generate a random boolean value.

Parameters: None.


### randemail

Generate a random email address (which will be useless for sending email - probably).

Parameters:

*   `domain`:
    Tail end part of the email adddress. Default: <dodgy.zz>

### randfloat

Generate a random float from a uniform distribution.

Parameters:

*   `min`:
    Minimum value. Default: smallest 32 bit signed integer.

*   `max`:
    Maximum value. Default: largest 32 bit signed integer.

*   `precision`:
    Round to this many decimal places. If not specified then no rounding is done.

Note that the min/max limits are somewhat arbitrary for floats.


### randint

Generate a random integer.

Parameters:

*   `min`:
    Minimum value. Default: smallest 32 bit signed integer.

*   `max`:
    Maximum value. Default: largest 32 bit signed integer.

### randphone

Generate a random phone number. The phone number is not guaranteed to be a
legitimate phone number according to the country numbering plan - it just
looks the part.

Parameters:

*   `country`:
    A 2 letter ISO 3166 country code. Currently the only supported value is <AU>.
    Default: <AU>.

*   `style`:
    Formatting style. One of <fixed> or <mobile>, although <cell> and <cellular>
    are accepted as synonyms of the latter. If not specified, then a style is
    selected at random on each invocation.

*   `international`:
    If <yes> then the number will be in the <+country_code nnnn> format. Default: <no>.

### randstr

Generate a random string.

Parameters:

*   `min`:
    Minimum string length. Default: <1>.

*   `max`:
    Maximum string length. Default: <20>.

*   `case`:
    One of <upper>, <lower>, <initcap>, <mixed>. Default: <mixed>.

### select

Select a random value from a list. The list can be supplied in different
ways, controlled by the parameters.

Exactly one of the following parameters is required:

*   `values`:
    A non-empty list of static values. Useful for small selection lists.

*   `file`:
    The name of a text file containing selection values - one entry per line.
    Empty lines and lines starting with # are ignored. The entire file
    is cached in memory so this is useful for small to medium size lists.

*   `dbase`:
    The name of an sqlite3 database file containing the list. If this
    option is used then the `table` and `column` parameters are also required.
    This is useful for large lists.

If the `dbase` parameter is used, then the following are also required:

*   `table` *:
    Name of the table or containing the selection list.

*   `column` *:
    Name of the column in the table containing the selection list.

If multiple columns of the selection database are being used to generate
multiple output fields, then the same selection database record will be
used for each usage in a given output record. For example, if <PlaceName>
and <PostCode> columns are used to generate corresponding fields in the
output, the selected <PostCode> will match the <PlaceName> in each case.

It is possible to use a view in place of a table for the `table` parameter
but the view _must_ contain a <rowid> column. Create the view thus:

    CREATE VIEW x AS SELECT rowid, * FROM basetable;

There may be performance impacts using a view. Try creating
some indexes on the base table to help or alternatively just create a
new table from the base table containing the data you want.

### serial

Generate the next serial number in a sequence. Sequences are named and are
disjoint.

Parameters:

*   `name`:
    Serial sequence name. Serials with different names are independent. If
    not supplied a default name is used.

*   `start`:
    Initial value of the serial. Default 0.

*   `incr`:
    Serial number increment. Must be non-zero. Default 1.

## EXAMPLE

See `sample1.yaml` provided with the source for a sample schema specification file.

## DATA SOURCES

The following are useful sources of data:

*   `http://www.geonames.org`:
    The GeoNames geographical database covers all countries and contains
    over eight million placenames that are available for download free of charge.

In addition, the following data sets are included with dodgy:

*   `PlaceNames_AU.db`:
    Database of placenames for Australia with separate views for each state.
    Check the schema using:

    sqlite3 PlaceNames_AU.db .schema

*   `PeopleNames.db`:
    A selection of 200 boy and 200 girl names and 1000 family names suitable
    for use with a `select` generator. Check the schema using:

    sqlite3 PeopleNames.db .schema

## ENVIRONMENT

*   `DODGYPATH`:
    Specify the path for locating source data files used by generators. Can be
    overridden by the `-p`, `--path` command line option and the `path` key in
    the configuration file.

## WARNINGS

The random number generator used by dodgy is not suitable for any
cryptographic or security related purpose.

## SEE ALSO

geonames2db(1) converts GeoNames (<http://www.geonames.org>) source data
files into a format that can be used by dodgy.

## LICENCE

Apache 2.0.

## AUTHOR

Murray Andrews
