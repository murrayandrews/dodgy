# Common makefile for the toolbox. This needs to be 'included' in a Makefile.
# The includer must define the PROGRAMS variable.

#-------------------------------------------------------------------------------
# Install dirs, programs and common config items

LIB = /usr/local/lib
BIN = /usr/local/bin
MAN = /usr/local/share/man

MAN_SECTION = 1

LIBMODE = 644
BINMODE = 755
MANMODE = 644

INSTALL = sudo install -S -v
MD_TO_MAN = ronn --roff --organization MA --manual ToolBox --date `date +'%Y-%m-%d'`

#-------------------------------------------------------------------------------
# Derived vars and common rules

BINFILES=$(addprefix $(BIN)/,$(PROGRAMS))
LOCAL_MANFILES=$(addsuffix .$(MAN_SECTION),$(PROGRAMS))
MANFILES=$(addprefix $(MAN)/man$(MAN_SECTION)/,$(LOCAL_MANFILES))


$(BIN)/%: %
	$(INSTALL) -m $(BINMODE) $< $@

$(BIN)/%: %.py
	$(INSTALL) -m $(BINMODE) $< $@

$(BIN)/%: %.sh
	$(INSTALL) -m $(BINMODE) $< $@

$(MAN)/man*/%: %
	$(INSTALL) -m $(MANMODE) $< $@

%.$(MAN_SECTION): %.md
	$(MD_TO_MAN) $<

#-------------------------------------------------------------------------------
# Global targets are exposed at the parent directory level. Locals are not.
GLOBAL_TARGETS = install force bin man iman clean
LOCAL_TARGETS = help gitclean lib iman

.PHONY: $(GLOBAL_TARGETS) $(LOCAL_TARGETS)

help:	# Default target
	@echo "Available targets:"
	@echo "    help: This message"
	@echo "    install: Install everything (git status must be clean)"
	@echo "    force: Install everything (even if git status is not clean)"
	@echo "    bin: Install programs only (even if git status is not clean)"
	@echo "    iman: Install manual entries"
	@echo "    man: Create manual entries locally"
	@echo "    clean: Remove files that can be regenerated"

gitclean:
	@if [ "`git status -s`" != "" ]; \
		then \
				echo "git status is not clean - cannot install"; \
				exit 1; \
		fi

install: gitclean force  # Only install if git status is clean

force:	bin iman  # Install even if git is not clean

bin:	$(BINFILES)  # Install programs

lib:	$(LIBFILES)

man:	$(LOCAL_MANFILES)  # Create man files locally

iman:	$(MANFILES)  # Install man files

clean:	
	$(RM) $(LOCAL_MANFILES)

__global_targets__:
	@echo $(GLOBAL_TARGETS)
