#!/usr/bin/env python

"""
Convert a GeoNames (http://www.geonames.org) source data file into an SQLite
database.

Find source data files here: http://download.geonames.org/export/zip/

This is from the GeoNames readme file explaining the format:

country code      : iso country code, 2 characters
postal code       : varchar(20)
place name        : varchar(180)
admin name1       : 1. order subdivision (state) varchar(100)
admin code1       : 1. order subdivision (state) varchar(20)
admin name2       : 2. order subdivision (county/province) varchar(100)
admin code2       : 2. order subdivision (county/province) varchar(20)
admin name3       : 3. order subdivision (community) varchar(100)
admin code3       : 3. order subdivision (community) varchar(20)
latitude          : estimated latitude (wgs84)
longitude         : estimated longitude (wgs84)
accuracy          : accuracy of lat/lng from 1=estimated to 6=centroid

The table will have any existing data removed for all country codes present
in the input. This should make the operation idempotent.

All rights reserved (Murray Andrews 2016)

See licence.txt

"""

from __future__ import print_function
import sys
import csv
import sqlite3
import re
from os.path import basename

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])

TABLE_NAME = 'PlaceNames'

fields = [
    ['CountryCode', 'VARCHAR(2)'],
    ['PostCode', 'VARCHAR(20)'],
    ['PlaceName', 'VARCHAR(180)'],
    ['AdminName1', 'VARCHAR(100)'],
    ['AdminCode1', 'VARCHAR(20)'],
    ['AdminName2', 'VARCHAR(100)'],
    ['AdminCode2', 'VARCHAR(20)'],
    ['AdminName3', 'VARCHAR(100)'],
    ['AdminCode3', 'VARCHAR(20)'],
    ['Latitude', 'FLOAT'],
    ['Longitude', 'FLOAT'],
    ['Accuracy', 'INTEGER'],
]

added_countries = set()


# ------------------------------------------------------------------------------
def create_table(cursor):
    """
    Create the PlaceNames table.

    :param cursor:      Database cursor
    :type cursor:       sqlite3.Cursor
    """

    column_defs = ', '.join([' '.join(x) for x in fields])
    cursor.execute('CREATE TABLE IF NOT EXISTS {}({});'.format(TABLE_NAME, column_defs))


# ------------------------------------------------------------------------------
def delete_entries(cursor, country_code):
    """
    Delete entries with the specified country code.

    :param cursor:      Database cursor
    :param country_code: Country code.

    :type cursor:       sqlite3.Cursor
    :type country_code: str

    :return:            The number of entries deleted.
    :rtype:             int
    """

    # See if there is anything to delete

    cursor.execute(
        'SELECT count(*) FROM {} WHERE {}=?'.format(TABLE_NAME, fields[0][0]),
        (country_code,)
    )
    n = cursor.fetchone()[0]

    # Delete the entries
    if n:
        cursor.execute(
            'DELETE FROM {} WHERE {}=?'.format(TABLE_NAME, fields[0][0]),
            (country_code,)
        )

    return n


# ------------------------------------------------------------------------------
def insert_record(cursor, data):
    """
    Insert the given record.

    :param cursor:      Database cursor
    :param data:        A list of data items.

    :type cursor:       sqlite3.Cursor
    :type data:         list

    """

    cursor.execute(
        'INSERT INTO {} VALUES ({})'.format(TABLE_NAME, ','.join(['?'] * len(data))),
        tuple(data)
    )


# ------------------------------------------------------------------------------
def process_input(cursor, stream):
    """
    Process data from the specified stream.

    :param cursor:      Database cursor
    :param stream:      Input data stream

    :type cursor:       sqlite3.Cursor
    :type stream:       file

    :return:            A tuple: (lines read, insert count)
    :rtype:             tuple(int, int)
    """

    line_count = 0
    insert_count = 0

    for data in csv.reader(stream, delimiter='\t'):
        line_count += 1

        if len(data) != len(fields):
            print('Field count mismatch on line {} - skipping'.format(line_count),
                  file=sys.stderr)
            continue

        # Delete any existing entries for this country (on first occurrence)
        if data[0] not in added_countries:
            n = delete_entries(cursor, data[0])
            if n:
                print('Deleting {} entries for {} {}'.format(n, fields[0][0], data[0]),
                      file=sys.stderr)
        added_countries.add(data[0])

        insert_record(cursor, data)
        insert_count += 1

    return line_count, insert_count


# ------------------------------------------------------------------------------
def create_views(cursor):
    """
    Create a view for each added country.

    :param cursor:      Database cursor
    :type cursor:       sqlite3.Cursor
    """

    for c in added_countries:
        try:
            cc = re.sub(r'[^\w]+', '', c)  # Remove any garbage
            cursor.execute(
                'CREATE VIEW IF NOT EXISTS {}_{} AS SELECT rowid, * FROM {} WHERE {}="{}"'.format(
                    TABLE_NAME, cc, TABLE_NAME, fields[0][0], cc
                )
            )
        except Exception as e:
            print(type(e), e)
            raise


# ------------------------------------------------------------------------------
def drop_indexes(cursor):
    """
    Drop index on country codes. Any manually created indexes are untouched.

    :param cursor:      Database cursor
    :type cursor:       sqlite3.Cursor
    """

    cursor.execute(
        'DROP INDEX IF EXISTS IDX_{table}_{column}'.format(
            table=TABLE_NAME, column=fields[0][0]
        )
    )


# ------------------------------------------------------------------------------
def create_indexes(cursor):
    """
    Create an index on country codes. Other indexes will need to be created
    manually.

    :param cursor:      Database cursor
    :type cursor:       sqlite3.Cursor
    """

    cursor.execute(
        'CREATE INDEX IF NOT EXISTS IDX_{table}_{column} ON {table} ({column})'.format(
            table=TABLE_NAME, column=fields[0][0]
        )
    )


# ------------------------------------------------------------------------------
def main():
    # Create the table

    conn = sqlite3.connect(sys.argv[1])
    conn.text_factory = str  # Munge unicode
    cursor = conn.cursor()

    try:
        create_table(cursor)
        drop_indexes(cursor)
        line_count, insert_count = process_input(cursor, sys.stdin)
        create_views(cursor)
        create_indexes(cursor)
    except Exception as e:
        print('{}: {}. Rolling back.'.format(PROG, e), file=sys.stderr)
        conn.rollback()
        return 2
    else:
        conn.commit()
        cursor.execute('VACUUM')
        print('{} lines read. {} entries added.'.format(line_count, insert_count),
              file=sys.stderr)
    finally:
        conn.close()

    return 0


# ------------------------------------------------------------------------------
if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('Usage: {} sqlite-db-file < geonames_data_file'.format(PROG), file=sys.stderr)
        exit(1)

    exit(main())
